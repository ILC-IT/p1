const express = require('express')
const app = express()
const port = 8023
const libros = [
    { titulo: "Sapiens", autor: "Yuval Noah Harari"},
    { titulo: "Homo Deus", autor: "Yuval Noah Harari"},
    { titulo: "El evangelio según Jesucristo", autor: "José Saramago"},
    { titulo: "El viaje del Elefante", autor: "José Saramago"},
    { titulo: "Fundación", autor: "Isaac Asimov"},
    { titulo: "Yo Robot", autor: "Isaac Asimov"}
];
const usuarios = [
    {
      "id": 1,
      "name": "Leanne Graham",
      "username": "Bret",
      "email": "Sincere@april.biz",
      "address": {
        "street": "Kulas Light",
        "suite": "Apt. 556",
        "city": "Gwenborough",
        "zipcode": "92998-3874",
        "geo": {
          "lat": "-37.3159",
          "lng": "81.1496"
        }
      },
      "phone": "1-770-736-8031 x56442",
      "website": "hildegard.org",
      "company": {
        "name": "Romaguera-Crona",
        "catchPhrase": "Multi-layered client-server neural-net",
        "bs": "harness real-time e-markets"
      }
    },
    {
      "id": 2,
      "name": "Ervin Howell",
      "username": "Antonette",
      "email": "Shanna@melissa.tv",
      "address": {
        "street": "Victor Plains",
        "suite": "Suite 879",
        "city": "Wisokyburgh",
        "zipcode": "90566-7771",
        "geo": {
          "lat": "-43.9509",
          "lng": "-34.4618"
        }
      },
      "phone": "010-692-6593 x09125",
      "website": "anastasia.net",
      "company": {
        "name": "Deckow-Crist",
        "catchPhrase": "Proactive didactic contingency",
        "bs": "synergize scalable supply-chains"
      }
    },
];

//esto es para parsear el body
app.use(express.urlencoded({ extended: true}))
app.use(express.json({limit:'5Mb'}))

app.get('/', (req, res) => { 
    res.send('Hola Mundo probando Gitlab')
})

app.get('/libros', (req, res) => { 
    res.send(libros)
})

app.get('/usuarios', (req, res) => { 
    res.send({num_usuarios:usuarios.length, usuarios:usuarios})
})

app.get('/usuarios/:id', (req, res) => { 
    // res.send({id:req.params.id, midato:req.query.midato})
    res.send(usuarios[req.params.id]) //{{miapi_local}}/usuarios/0
})

app.use(function(req, res, next) { 
    req.body.datos.usuario = "Ulises";
next();
})

app.post('/enviodatos', (req, res) => { 
    res.send({datos:req.body.datos})
})

//Middleware, este ponerlo al final
app.use(function(req, res, next) { 
    res.status(404).send('No existe la ruta')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})